from pprint import pprint
# читаем адресную книгу в формате CSV в список contacts_list
import csv
import re

with open("phonebook_raw.csv", encoding='utf-8') as f:
    rows = csv.reader(f, delimiter=",")
    contacts_list = list(rows)
    # pprint(contacts_list)
# TODO 1: выполните пункты 1-3 ДЗ
# ваш код
processed_contact_dict = dict()
result_contact_list = list()
num_duplicates = 0

for index_contact in range(len(contacts_list)):

    # 0-lastname, 1-firstname, 2-surname, 3-organization, 4-position, 5-phone, 6-email
    temp_attr = ['', '', '', '', '', '', '']
    temp_fio = ""

    contact = contacts_list[index_contact]

    if index_contact == 0:
        result_contact_list.append(contact)
        continue

    for i in range(len(contact)):
        if i < 3 and contact[i] is not None:
            temp_fio += contact[i] + " " if i == 2 or 3 else ""
        elif i == 5 and contact[i] != "":
            temp_attr[i] = re.sub(r'[\(\)\s-]', '', contact[i])

            if 'доб' in temp_attr[i]:
                pattern = re.compile(r"(\+7|8)\(?(\d{3}?)(\d{3}?)(\d{2}?)(\d{2}?)(доб.\d+)")
                temp_attr[i] = pattern.sub(r"+7(\2)\3-\4-\5 \6", temp_attr[i])
            else:
                pattern = re.compile(r"(\+7|8)\(?(\d{3}?)(\d{3}?)(\d{2}?)(\d{2}?)")
                temp_attr[i] = pattern.sub(r"+7(\2)\3-\4-\5", temp_attr[i])

        elif i in [3, 4, 6] and contact[i] != "":
            temp_attr[i] = contact[i]
        else:
            pass

    fio = temp_fio.split()

    temp_attr[0], temp_attr[1], temp_attr[2] = fio[0], fio[1], fio[2] if len(fio) == 3 else ""

    if temp_attr[0] + temp_attr[1] not in processed_contact_dict.keys():
        processed_contact_dict[temp_attr[0] + temp_attr[1]] = index_contact - num_duplicates
        result_contact_list.append([temp_attr[0], temp_attr[1], temp_attr[2], temp_attr[3],
                                    temp_attr[4], temp_attr[5], temp_attr[6]])

    else:
        current_update_contact = result_contact_list[processed_contact_dict[temp_attr[0] + temp_attr[1]]]

        for i in range(len(current_update_contact)):
            if current_update_contact[i] == "" and temp_attr[i] != "":
                current_update_contact[i] = temp_attr[i]

        num_duplicates += 1

# TODO 2: сохраните получившиеся данные в другой файл
# код для записи файла в формате CSV
with open("phonebook.csv", "w", encoding="utf-8", newline='') as f:
    datawriter = csv.writer(f, delimiter=',')
    # Вместо contacts_list подставьте свой список
    datawriter.writerows(result_contact_list)
