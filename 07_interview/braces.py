from stack import Stack


def is_only_braces(input_string: str):
    for symbol in input_string:
        if symbol not in ['(', ')', '[', ']', '{', '}']:
            return False
    return True


def check_mirror_brace(first_brace: str, second_brace: str):
    if first_brace == '(' and second_brace == ')':
        return True
    elif first_brace == '[' and second_brace == ']':
        return True
    elif first_brace == '{' and second_brace == '}':
        return True
    else:
        return False


def is_braces_balance(input_string: str):
    if is_only_braces(input_string):
        size = len(input_string)
        if size % 2 != 0:
            return "Несбалансированно"
        else:
            temp_stack = Stack()
            for i in range(size):
                if input_string[i] not in [']', ')', '}']:
                    temp_stack.push(input_string[i])
                else:
                    if not check_mirror_brace(temp_stack.pop(), input_string[i]):
                        return "Несбалансированно"
            return "Сбалансировано"

    else:
        return "В строке есть символы не скобки!"


if __name__ == "__main__":
    brace_string = input("введите последовательность скобок: ")
    print(is_braces_balance(brace_string))
