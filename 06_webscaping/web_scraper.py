import requests
from bs4 import BeautifulSoup


def search_keyword(keyword_list: list, text: str):
    for word in keyword_list:
        if word in text:
            return True
    return False


if __name__ == "__main__":
    KEYWORD = ['iot', 'python', 'хакер', 'безопасность']
    URL = "https://habr.com/ru/all"

    result_list = list()

    r = requests.get(URL)
    soup = BeautifulSoup(r.text, 'html.parser')
    previews = soup.find_all('article', class_="post_preview")

    for post_preview in previews:
        post_body = post_preview.find('div', class_="post__text")
        post_title = post_preview.find('a', class_="post__title_link")
        post_time = post_preview.find('span', class_="post__time").text
        post_title_link = post_title.attrs['href']
        post_title_text = post_title.text
        text_preview = post_body.text.strip().lower()

        if search_keyword(KEYWORD, text_preview):
            result_list.append((post_time, post_title_text, post_title_link))

    for result in result_list:
        print(f"Дата - {result[0]} - {result[1]} - {result[2]}")
