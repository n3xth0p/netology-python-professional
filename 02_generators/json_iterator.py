import json
import os.path


class FileIterator:
    def __init__(self, filename:str):
        self.filename =  filename
        if os.path.exists(filename):
            with open(filename, "r", encoding="utf-8") as file:
                self.content = json.load(file)
                self.start = -1
                self.end = len(self.content) - 1
        else:
            raise FileExistsError

    def __iter__(self):
        return self
    
    def __next__(self):
        self.start += 1
        if self.start == self.end:
            raise StopIteration
        return self.content[self.start]['translations']['rus']['common']
    
    def make_links(self, filename:str):
        with open(filename, "w+", encoding="utf-8") as file:
            for item in self:
                file.write(f"http://ru.wikipedia.org/wiki/{item.replace(' ','_')}\n")
        

if __name__ == "__main__":
    FILENAME = "countries.json"
    OUTPUT = "links.txt"
    my_iter = FileIterator(FILENAME)
    my_iter.make_links(OUTPUT)
