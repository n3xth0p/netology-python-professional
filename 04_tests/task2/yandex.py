import requests

class YaDisk:
    def __init__(self, token: str):
        self.token = token
        self.HEADERS = {
            "Authorization": f"OAuth {self.token}"
        }
        self.URL = "https://cloud-api.yandex.net/v1/disk/resources"

    def check_if_exists(self, name: str):
        response = requests.get(
            self.URL,
            params={
                "path": name
            },
            headers=self.HEADERS
        )

        return response.status_code

    def create_dir(self, path):
        response = requests.put(
            self.URL,
            params={
                "path": path,
                "templated": "true"
            },
            headers=self.HEADERS
        )
        return response.status_code

    def delete_dir(self, path):
        response = requests.delete(
            self.URL,
            params={
                "path": path
            },
            headers=self.HEADERS
        )
        return response.status_code