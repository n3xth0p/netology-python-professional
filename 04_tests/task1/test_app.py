import unittest
from unittest.mock import patch

from app import check_document_existance, get_doc_owner_name, delete_doc, add_new_doc


class TestGetDocOwnerName(unittest.TestCase):

    @patch('builtins.input', side_effect=["2207 876234"])
    def test_get_doc_owner_name_equal(self, mock_inputs):
        result = get_doc_owner_name()
        self.assertEqual("Василий Гупкин", result)

    @patch('builtins.input', side_effect=["11-2"])
    def test_get_doc_owner_name_not_equal(self, mock_inputs):
        result = get_doc_owner_name()
        self.assertNotEqual("Василий Гупкин", result)

    @patch('builtins.input', side_effect=["123"])
    def test_get_doc_owner_name_none(self, mock_inputs):
        result = get_doc_owner_name()
        self.assertFalse(result)


class TestCheckDocumentExistance(unittest.TestCase):

    def test_check_document_existance_true(self):
        self.assertTrue(check_document_existance("11-2"))

    def test_check_document_existance_false(self):
        self.assertFalse(check_document_existance("123"))


class TestAddNewDoc(unittest.TestCase):

    @patch('builtins.input', side_effect=["123", "invoice", "Иванов Иван", "3"])
    def test_add_new_doc_success(self, mock_inputs):
        result = add_new_doc()
        self.assertEqual("3", result)
        self.assertTrue(check_document_existance("123"))


class TestDeleteDoc(unittest.TestCase):

    @patch('builtins.input', side_effect=["11-2"])
    def test_delete_doc_success(self, mock_inputs):
        result = delete_doc()
        self.assertEqual(("11-2", True), result)
        self.assertFalse(check_document_existance("11-2"))
