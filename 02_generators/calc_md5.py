import hashlib

def calculate_md5(filename:str):
    file = open(filename, encoding="utf-8")
    while True:
      line = file.readline()
      if not line:
        file.close()
        break
      yield hashlib.md5(line.strip().encode("utf-8")).hexdigest()


if __name__=="__main__":
    FILENAME = "links.txt"
    content = calculate_md5(FILENAME)

    for item in content:
        print(item)