import email
import smtplib
import imaplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class Email:
    def __init__(self, login: str, password: str):
        self.login = login
        self.password = password

    def send(self, smtp_server_address: str, smtp_server_port: int, recipients: list, subject: str, text: str):
        message = MIMEMultipart()
        message['From'] = self.login
        message['To'] = ', '.join(recipients)
        message['Subject'] = subject
        message.attach(MIMEText(text))

        connection = smtplib.SMTP(smtp_server_address, smtp_server_port)
        # identify ourselves to smtp gmail client
        connection.ehlo()
        # secure our email with tls encryption
        connection.starttls()
        # re-identify ourselves as an encrypted connection
        connection.ehlo()

        connection.login(self.login, self.password)
        connection.sendmail(self.login, connection, message.as_string())
        connection.quit()

    def receive(self, imap_server_address: str, header: str = None):
        connection = imaplib.IMAP4_SSL(imap_server_address)
        connection.login(self.login, self.password)
        connection.list()
        connection.select("inbox")
        criterion = '(HEADER Subject "%s")' % header if header else 'ALL'
        result, data = connection.uid('search', None, criterion)
        assert data[0], 'There are no letters with current header'
        latest_email_uid = data[0].split()[-1]
        result, data = connection.uid('fetch', latest_email_uid, '(RFC822)')
        raw_email = data[0][1]
        email_message = email.message_from_string(raw_email)
        connection.logout()


if __name__ == "__main__":
    LOGIN = 'login@gmail.com'
    PASSWORD = 'qwerty'
    GMAIL_SMTP = "smtp.gmail.com"
    GMAIL_SMTP_PORT = 587
    GMAIL_IMAP = "imap.gmail.com"
    RECEPIENTS = ['vasya@email.com', 'petya@email.com']
    SUBJECT = 'Subject'
    MESSAGE = 'Message'

    my_email = Email(LOGIN, PASSWORD)
    my_email.send(GMAIL_SMTP, GMAIL_SMTP_PORT, RECEPIENTS, SUBJECT, MESSAGE)
    my_email.receive(GMAIL_IMAP)
