from datetime import datetime

def simple_logger(old_function):
    def new_function(*args, **kwargs):
        result = old_function(*args, **kwargs)
        filename = f"{old_function.__name__}.log"
        with open(filename, "a+", encoding="utf-8") as file:
            file.write(f"{datetime.now()} вызвана функция {new_function.__name__} с параметрами {args} {kwargs}, "
                       f"результат {result}\n")
        return result
    return new_function


def logger_to_file_fabric(logfile:str):

    def logger(old_function):
        def new_function(*args, **kwargs):
            result = old_function(*args, **kwargs)
            filename = f"{logfile}"
            with open(filename, "a+", encoding="utf-8") as file:
                file.write(f"{datetime.now()} вызвана функция {new_function.__name__} с параметрами {args} {kwargs}, "
                        f"результат {result}\n")
            return result
        return new_function
    
    return logger

#@simple_logger
@logger_to_file_fabric('mylog.log')
def calculate_md5(filename: str):
    file = open(filename, encoding="utf-8")
    while True:
        line = file.readline()
        if not line:
            file.close()
            break
        yield hashlib.md5(line.strip().encode("utf-8")).hexdigest()




if __name__ == "__main__":
    FILENAME = "links.txt"
    content = calculate_md5(FILENAME)
