class Stack:
    def __init__(self):
        self.elements = list()

    def is_empty(self):
        return True if len(self.elements) == 0 else False

    def size(self):
        return len(self.elements)

    def push(self, new_element):
        self.elements.append(new_element)

    def pop(self):
        if self.is_empty():
            return "stack is empty"
        else:
            top_element = self.elements[-1]
            del self.elements[-1]
            return top_element

    def peek(self):
        return self.elements[-1]


if __name__ == "__main__":
    my_stack = Stack()
    print(my_stack.is_empty())

    my_stack.push(10)
    my_stack.push(10)
    my_stack.push(11)
    my_stack.push(2)
    my_stack.push(33)
    my_stack.push("testsrt")

    print(my_stack.size())
    print(my_stack.is_empty())
    print(my_stack.peek())
    print(my_stack.is_empty())
    print("size - ", my_stack.size())
    print(my_stack.pop())
    print(my_stack.pop())
    print(my_stack.pop())
    print(my_stack.pop())
    print(my_stack.pop())
    print(my_stack.pop())
    print(my_stack.pop())
    print(my_stack.pop())
    print(my_stack.pop())


