import pytest
from yandex import YaDisk


class TestYaDisk:

    def setup(self):
        print("setup test")
        self.VALID_TOKEN = ""  # enter valid token for yandex disk
        self.NOT_VALID_TOKEN = "123"  # enter not valid token for yandex disk
        self.DIR_NAME = "mytestdir"
        self.valid_disk = YaDisk(self.VALID_TOKEN)
        self.not_valid_disk = YaDisk(self.NOT_VALID_TOKEN)

    def test_create_dir_success(self):
        assert self.valid_disk.create_dir(self.DIR_NAME) == 201
        assert self.valid_disk.check_if_exists(self.DIR_NAME) == 200

    def test_create_dir_already_created(self):
        assert self.valid_disk.create_dir(self.DIR_NAME) == 201
        assert self.valid_disk.create_dir(self.DIR_NAME) == 409

    def test_create_dir_unsucces(self):
        assert self.not_valid_disk.create_dir(self.DIR_NAME) not in [200, 201]

    def teardown(self):
        print("teardown test")
        self.valid_disk.delete_dir(self.DIR_NAME)