from datetime import date
from application.salary import calculate_salary
from db.people import get_employees

if __name__== "__main__":
    print(date.today())
    print("start: main module")
    print(__name__)
    calculate_salary()
    get_employees()
    print("end: main module")